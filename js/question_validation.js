var app = angular.module('myApp', []);

app.controller('myCtrl', function ($scope, $http) {
	// Fetch Question data
	$http.get("get_questions_of_unvalidated_ans.php")
		.then(function (response) {
			$scope.questions = response.data.records;
			console.log('questions.json', $scope.questions);
		});
	
	$scope.Answers = {};

	 $scope.change_flag = function (answerid) {
  console.log("aid "+answerid);
        
    $http({
				method: "POST",
				url: "validate_answer",
				data: { answerid: answerid },
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
				.then(function (response) {
					console.log("flag changed succesfully");
				});


   
    }
    
    $scope.viewSolution = function (qid) {
  console.log("qid "+qid);
        if (!$scope.questions[qid - 1].hasOwnProperty('answers')) {
    $http({
				method: "POST",
				url: "show_unvalidated_answers.php",
				data: { questionid: qid },
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
				.then(function (response) {
					
					
					$scope.questions[qid - 1].answers = response.data.records;
					console.log("Q", $scope.questions[qid - 1]);
				});


    }

        $scope.viewXpln(qid);
    }
    
  	







	// Hide options of "qId" Question which has "normal" class
	$scope.hideBtn = function (qId) {
		//if($("#"+qId).find(".ques-options").find('.ques-opt-btn').hasClass('normal')){
		$("#" + qId).find(".ques-options").find('.normal').removeClass('normal').addClass("hidden");
		//}
	}

	// Unhide specific options which has id as "oId"
	$scope.unhideBtn = function (oId) {
		$("#" + oId).removeClass("hidden");
	}

	//  Mark this option correct
	$scope.correctBtn = function (oId) {
		$scope.unhideBtn(oId);
		$("#" + oId).addClass("selected correct");
	}

	//  Mark this option wrong
	$scope.wrongBtn = function (oId) {
		$scope.unhideBtn(oId);
		$("#" + oId).addClass("selected wrong");
	}

	//  This will be called whenever "View Solution" button is clicked
	$scope.viewXpln = function (qId) {
		///// Find the answer id of "qId" Question and Mark it correct
		angular.forEach($scope.questions, function (value, key) {
			if (value.id == qId) {
				// generate id to mark option Correct id pattern=([question_id][option_id])
				var selector = qId + '' + value.answer.id;

				$scope.hideBtn(qId); /// Hide other options
				$scope.correctBtn(selector);
			}
		});

		// handle sliding Up and Down of Explanation Box
		if ($("#" + qId).find(".ques-xpln-toggle").hasClass('showing')) /// Close
		{
			$("#" + qId).find(".ques-xpln-toggle").removeClass('showing');
			$("#" + qId).find(".xpln-toggler").removeClass("fas fa-angle-double-up").text("View Solutions");
			$("#" + qId).find(".xpln-body").slideUp();
		}
		else // Open
		{
			//    First hide all other explanation section
			$(".ques-xpln-toggle").removeClass('showing');
			$(".xpln-toggler").removeClass("fas fa-angle-double-up").text("View Solutions");
			$(".xpln-body").slideUp();
			//---------------------------------------------

			// show this explanation section --------------
			$("#" + qId).find(".ques-xpln-toggle").addClass('showing');
			$("#" + qId).find(".xpln-toggler").addClass("fas fa-angle-double-up").text("");
			$("#" + qId).find(".xpln-body").slideDown();
		}
	}

	//  This will be called when any option is clicked
	$scope.evaluate = function (opt_json_id, question_id, opt_sel_id) {
		$("#" + question_id).addClass("attempt");
		angular.forEach($scope.questions, function (value, key) {
			if (value.id == question_id) {

				//// Selected option is Correct So change its Styling

				if (value.answer.id == opt_json_id) {
					//*************************************************
					//       Any scoring for MCQs will be done here
					//***************************************************

					$scope.hideBtn(question_id); /// Hide other options
					$scope.correctBtn(opt_sel_id); /// Show this option
					$scope.viewXpln(question_id);
				}
				else {
					var corBtn = question_id + '' + value.answer.id;
					// $scope.hideBtn(question_id); /// Hide other options
					$("#" + opt_sel_id).removeClass('normal');

					$scope.wrongBtn(opt_sel_id); /// Wrong
				}
			}
		});
	}
});

$(function () {
	$(window).on('scroll', function (event) {
		var scrollValue = $(window).scrollTop();
		$("#sc").text(scrollValue + "px");
		if (scrollValue > 55) {
			$('.nameBar').addClass('affix');
		}
		else {
			$('.nameBar').removeClass('affix');
		}
	});
});



function senddata(some) {
}
// fired whenever Any option is clicked
function optionClicked(some) {

	// Fetch the data-attribute values from this "some" element
	var qid = $(some).data("question-id"); // question id
	var id = $(some).data("my-id"); // Fetch option id for json data checking
	var oid = $(some).attr('id');   // Fetch this "some" element ID

	var scope = angular.element(document.getElementById("myBody")).scope();
	scope.$apply(function () {
		scope.evaluate(id, qid, oid);
	});
}