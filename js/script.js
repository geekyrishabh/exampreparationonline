"use strict"

  

  $(function(){
    $(window).on('scroll', function(event)
    {
      var scrollValue = $(window).scrollTop();
      $("#sc").text(scrollValue+"px");
      if (scrollValue > 55) {
        $('.nameBar').addClass('affix');
      } 
      else{
        $('.nameBar').removeClass('affix');
      }
    });
  });
  
  // fired whenever View Solution is clicked
  function xplnToggle(some){

    // Fetch Question Id from attribute of this "some" element 
    var qid = $(some).data("question-id");

    // Call viewXpln() angular function  
    var scope = angular.element(document.getElementById("myBody")).scope();
    scope.$apply(function () {
      scope.viewXpln(qid);
    });
  }

  // fired whenever Any option is clicked 
  function optionClicked(some){

    // Fetch the data-attribute values from this "some" element
    var qid = $(some).data("question-id"); // question id
    var id = $(some).data("my-id"); // Fetch option id for json data checking 
    var oid = $(some).attr('id');   // Fetch this "some" element ID 
    
    var scope = angular.element(document.getElementById("myBody")).scope();
    scope.$apply(function () {
      scope.evaluate(id,qid,oid);
    });
  }
function mathx(){
 //var val = document.getElementById('comment').value;
 var text = $("#comment").val();
  document.getElementById('eqn').innerHTML="<h3>"+"$$"+text+"$$"+"</h3>";
  MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
  //alert(text);
}

function appends(){
  console.log("working");
  var text = document.getElementById('comment');
  var select = document.getElementsByClassName('selectpicker');
  text.innerHTML=text.innerHTML+" "+select[0].value;
  mathx();
}
function preRender(){
	
	var text =$(".selectpicker").val();
	alert(text);
	//var text = "$$ f()x $$";
	//$("#preText").innerHTML= text;
	document.getElementById('preText').innerHTML ="<h4> $$"+text+"$$ </h4>";
	//document.getElementById('preTextHidden').innerHTML =text;
	$("#preTextHidden").val(text);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	//alert(text);
}
function addToBox(){
	var prev = $('#comment');
	//var next= prev.val();
	//prev.val('');
	var text = $("#preTextHidden").val();
	//alert(text);
	//prev.text(" "+text);
	 var r = document.getElementById('comment').innerHTML += " sjkka";
	 alert(r);
	mathx();
}

var app = angular.module('myApp', []);

  app.controller('myCtrl',function($scope,$http)
  {
    // Fetch Question data
    $http.get("get.php")
      .then(function(response){
        $scope.questions = response.data.records;
      });

    // Fetch Explanation data
    $http.get("explanation.json")          
      .then(function(response){
        $scope.explanation = response.data;
      });
    
    // Hide options of "qId" Question which has "normal" class
    $scope.hideBtn = function(qId)  
    {
      //if($("#"+qId).find(".ques-options").find('.ques-opt-btn').hasClass('normal')){
        $("#"+qId).find(".ques-options").find('.normal').removeClass('normal').addClass("hidden");
      //}
    }

    // Unhide specific options which has id as "oId" 
    $scope.unhideBtn = function(oId)   
    {
      $("#"+oId).removeClass("hidden");
    }

    //  Mark this option correct
    $scope.correctBtn = function(oId)      
    {
      $scope.unhideBtn(oId);
      $("#"+oId).addClass("selected correct");
    }

    //  Mark this option wrong
    $scope.wrongBtn = function(oId)  
    {
      $scope.unhideBtn(oId);
      $("#"+oId).addClass("selected wrong");
    }
    
    //  This will be called whenever "View Solution" button is clicked
    $scope.viewXpln = function(qId){
      ///// Find the anser id of "qId" Question and Mark it correct
      angular.forEach($scope.questions, function (value, key)
      {
        if(value.id == qId){

          // generate id to mark option Correct id pattern=([question_id][option_id])
          var selector = qId+''+value.answer.id;

          $scope.hideBtn(qId); /// Hide other options
          $scope.correctBtn(selector);
        }
      });

      // handle sliding Up and Down of Explanation Box 
      if($("#"+qId).find(".ques-xpln-toggle").hasClass('showing')) /// Close
      {
        $("#"+qId).find(".ques-xpln-toggle").removeClass('showing');
        $("#"+qId).find(".xpln-toggler").removeClass("fas fa-angle-double-up").text("View Solutions");
        $("#"+qId).find(".xpln-body").slideUp();
      }
      else // Open
      {
        //    First hide all other explanation section
        $(".ques-xpln-toggle").removeClass('showing');
        $(".xpln-toggler").removeClass("fas fa-angle-double-up").text("View Solutions");
        $(".xpln-body").slideUp();
        //---------------------------------------------

        // show this explanation section --------------
        $("#"+qId).find(".ques-xpln-toggle").addClass('showing');
        $("#"+qId).find(".xpln-toggler").addClass("fas fa-angle-double-up").text("");
        $("#"+qId).find(".xpln-body").slideDown();
      }
    }

    //  This will be called when any option is clicked
    $scope.evaluate = function(opt_json_id,question_id,opt_sel_id) {
      $("#"+question_id).addClass("attempt");
      angular.forEach($scope.questions, function (value, key) {
        if(value.id == question_id){

           //// Selected option is Correct So change its Styling

          if(value.answer.id == opt_json_id) 
            {
              //*************************************************
              //       Any scoring for MCQs will be done here
              //***************************************************

              $scope.hideBtn(question_id); /// Hide other options
              $scope.correctBtn(opt_sel_id); /// Show this option     
              $scope.viewXpln(question_id);
            }
          else {
            var corBtn = question_id+''+value.answer.id;
           // $scope.hideBtn(question_id); /// Hide other options
           $("#"+opt_sel_id).removeClass('normal');

            $scope.wrongBtn(opt_sel_id); /// Wrong
          } 
        }
      });
    }
  });