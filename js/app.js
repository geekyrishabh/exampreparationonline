var app = angular.module('myApp', []);

app.controller('myCtrl', function ($scope, $http) {
	// Fetch Question data
	
	$scope.Answers = {};
	$scope.submit_answer = function (qId) {
		console.log('submit_answer')
		//console.log($scope.Answers[qId]);
		//console.log(qId);
		$.post("postanswer.php",
			{
				question_id: qId, answer_value: $scope.Answers[qId]
			},
			function (data, status) {
				console.log("Data: " + data + "\nStatus: " + status);
			});
	}

$scope.my_questions = function () {
		 $http({
            method: "POST",
            url: "get_my_questions.php"
       
      })
            .then(function (response) {
                console.log('question', response.data.records);
                
             $scope.questions = response.data.records;
               
            });
		
	}



   $scope.submitfilter = function (interview1,interview2,interview3,interview4,interview5,recruitment,workdomain,internship,college) {

   	var interview = new Array();
   	


   	 var x1 = document.getElementById("hr").value;
   if(interview1==true)
   {
      interview.push(x1);
   }
   var x2= document.getElementById("gd").value;
   if(interview2==true)
   {
    interview.push(x2);  
   }
  
  var x3= document.getElementById("aptitude").value;
   if(interview3==true)
   {
interview.push(x3);
      
   }

var x4= document.getElementById("analytical").value;
   if(interview4==true)
   {
   	interview.push(x4);
      
   }
   
var x5= document.getElementById("core").value;
   if(interview5==true)
   {
      interview.push(x5);
   }
  

  var d = interview.concat(recruitment, workdomain,internship,college);
  console.log(d);
    $http({
            method: "POST",
            url: "get.php",
            data: { d:d }
       
      })
            .then(function (response) {
                console.log('question', response.data.records);
                //$scope.show_answers = response.data.records;
             $scope.questions = response.data.records;
                console.log("postfilters");
            });

       
        
    }

  
    $scope.viewSolution = function (qid) {
  console.log("qid "+qid);
        if (!$scope.questions[qid - 1].hasOwnProperty('answers')) {
    $http({
				method: "POST",
				url: "showanswer.php",
				data: { questionid: qid },
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
				.then(function (response) {
					
					
					$scope.questions[qid - 1].answers = response.data.records;
					console.log("Q", $scope.questions[qid - 1]);
				});


    }

      
    }
    
  	







	// Hide options of "qId" Question which has "normal" class
	$scope.hideBtn = function (qId) {
		//if($("#"+qId).find(".ques-options").find('.ques-opt-btn').hasClass('normal')){
		$("#" + qId).find(".ques-options").find('.normal').removeClass('normal').addClass("hidden");
		//}
	}

	// Unhide specific options which has id as "oId"
	$scope.unhideBtn = function (oId) {
		$("#" + oId).removeClass("hidden");
	}

	//  Mark this option correct
	$scope.correctBtn = function (oId) {
		$scope.unhideBtn(oId);
		$("#" + oId).addClass("selected correct");
	}

	//  Mark this option wrong
	$scope.wrongBtn = function (oId) {
		$scope.unhideBtn(oId);
		$("#" + oId).addClass("selected wrong");
	}

	//  This will be called whenever "View Solution" button is clicked
	$scope.viewXpln = function (qId) {
		///// Find the answer id of "qId" Question and Mark it correct
		angular.forEach($scope.questions, function (value, key) {
			if (value.id == qId) {
				// generate id to mark option Correct id pattern=([question_id][option_id])
				var selector = qId + '' + value.answer.id;

				$scope.hideBtn(qId); /// Hide other options
				$scope.correctBtn(selector);
			}
		});

		// handle sliding Up and Down of Explanation Box
		if ($("#" + qId).find(".ques-xpln-toggle").hasClass('showing')) /// Close
		{
			$("#" + qId).find(".ques-xpln-toggle").removeClass('showing');
			$("#" + qId).find(".xpln-toggler").removeClass("fas fa-angle-double-up").text("View Solutions");
			$("#" + qId).find(".xpln-body").slideUp();
		}
		else // Open
		{
			//    First hide all other explanation section
			$(".ques-xpln-toggle").removeClass('showing');
			$(".xpln-toggler").removeClass("fas fa-angle-double-up").text("View Solutions");
			$(".xpln-body").slideUp();
			//---------------------------------------------

			// show this explanation section --------------
			$("#" + qId).find(".ques-xpln-toggle").addClass('showing');
			$("#" + qId).find(".xpln-toggler").addClass("fas fa-angle-double-up").text("");
			$("#" + qId).find(".xpln-body").slideDown();
		}
	}

	//  This will be called when any option is clicked
	$scope.evaluate = function (opt_json_id, question_id, opt_sel_id) {
		$("#" + question_id).addClass("attempt");
		angular.forEach($scope.questions, function (value, key) {
			if (value.id == question_id) {

				//// Selected option is Correct So change its Styling

				if (value.answer.id == opt_json_id) {
					//*************************************************
					//       Any scoring for MCQs will be done here
					//***************************************************

					$scope.hideBtn(question_id); /// Hide other options
					$scope.correctBtn(opt_sel_id); /// Show this option
					$scope.viewXpln(question_id);
				}
				else {
					var corBtn = question_id + '' + value.answer.id;
					// $scope.hideBtn(question_id); /// Hide other options
					$("#" + opt_sel_id).removeClass('normal');

					$scope.wrongBtn(opt_sel_id); /// Wrong
				}
			}
		});
	}
});

$(function () {
	$(window).on('scroll', function (event) {
		var scrollValue = $(window).scrollTop();
		$("#sc").text(scrollValue + "px");
		if (scrollValue > 55) {
			$('.nameBar').addClass('affix');
		}
		else {
			$('.nameBar').removeClass('affix');
		}
	});
});



function senddata(some) {
}
// fired whenever Any option is clicked
function optionClicked(some) {

	// Fetch the data-attribute values from this "some" element
	var qid = $(some).data("question-id"); // question id
	var id = $(some).data("my-id"); // Fetch option id for json data checking
	var oid = $(some).attr('id');   // Fetch this "some" element ID

	var scope = angular.element(document.getElementById("myBody")).scope();
	scope.$apply(function () {
		scope.evaluate(id, qid, oid);
	});
}