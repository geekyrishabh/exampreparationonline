var app = angular.module('myApp', []);

app.controller('myCtrl', function ($scope, $http) {
	// Fetch Question data
	

   $scope.register1= function (name,email,password) {

 console.log(name);
     console.log(email);
     console.log(password);	
 
      $http({
				method: "POST",
				url: "register.php",
				data: { name: name,
				        email:email,
				        password:password   },
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
				.then(function (response) {
					console.log(response.data);
					
				
				});
        
    }

//login
   $scope.login1= function (email,password) {
     console.log(email);
     console.log(password);	
 
      $http({
				method: "POST",
				url: "login.php",
				data: {
				        email:email,
				        password:password   },
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
				.then(function (response) {
					console.log(response.data);
					
				
				});
        
    }



});






var formValid = false;
var passValid= false;
var emailValid= false;

var nameValid = false;
var signUpEmailValid = false;
var signUpPassValid = false;
var nameValid = false;
var signUpFormValid = false;

function chkFormValid(){
	var formValid = false;
	if(emailValid==true && passValid == true){
		formValid = true;
	}
	return formValid;
}
function chkSignUpFormValid(){
	var signUpFormValid = false;
	if(signUpEmailValid==true && signUpPassValid == true && nameValid==true){
		signUpFormValid = true;
	}
	return signUpFormValid;
}
function checkFill(some){
	var text=$(some).val();
	text=text.trim();
	if(text.length >0){
		$(some).addClass("filled");
	}
	else{
		$(some).removeClass("filled");
	}
}

///// Sign In Email check 
function checkSignInEmail(some){    
	checkFill(some);
	emailValid =  validateEmail(some);
}

/// Sign Up email check
function checkSignUpEmail(some){
	checkFill(some);
	signUpEmailValid =  validateEmail(some);
}

/// Chack name
function checkName(some){ //// Name Validater 
	checkFill(some);

	var name= $(some).val();
	name= name.trim();
	if(name.length==0){///Nothing entered
		$(some).val('');
		$(some).siblings(".msg").text("Name cannot be empty!").slideDown();
		$(some).removeClass('correct').addClass("wrong");
		nameValid = false;
	}
	else{
		var name_regex = /^[a-zA-Z ]*$/;
			if (name_regex.test(name)) {
			    $(some).siblings('.msg').slideUp();
			    $(some).siblings('.msgIcon').removeClass('fa-exclamation-circle').addClass('fa-check-circle').css("color","#005e00");
			    $(some).removeClass('wrong').addClass('correct');
			    nameValid = true;
			} else {
			    $(some).siblings('.msg').text("Only alphabets please !!").slideDown();
			    $(some).siblings('.msgIcon').removeClass('fa-check-circle').addClass('fa-exclamation-circle').css("color","#c62a2a");
			    $(some).removeClass('correct').addClass('wrong');
			    nameValid = false;
			}
	}
}

/// Sign Up pass check 
function checkSignUpPassword(some){
	checkFill(some);
	signUpPassValid = checkPass(some);
}

/////  Sign in pass check 
function checkSignInPassword(some){
	checkFill(some);
	passValid = checkPass(some);
}

function validateEmail(some) { //// Email Validate AND RETURN
    var email = $(some).val();
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
	if (re.test(email)) {
	    $(some).siblings('.msg').slideUp();
	    $(some).siblings('.msgIcon').removeClass('fa-exclamation-circle').addClass('fa-check-circle').css("color","#005e00");
	    $(some).removeClass('wrong').addClass('correct');
	    return true;
	} else {
	    $(some).siblings('.msg').text("Please enter a valid email address").slideDown();
	   $(some).siblings('.msgIcon').removeClass('fa-check-circle').addClass('fa-exclamation-circle').css("color","#c62a2a");
	    $(some).removeClass('correct').addClass('wrong');
	    return false;
	}
}

function checkPass(some){ ///validate Pass and return 
	var pass= $(some).val();
	pass=pass.trim();
	if(pass.length==0){///Nothing entered
		$(some).val('');
		$(some).siblings('.msg').text("Password cannot be empty!").slideDown();
		$(some).removeClass('correct').addClass("wrong");
		return false;
	}
	else{

	     var re1 = /[0-9]/;
	     var re2 = /[a-z]/;
	     var re3 = /[A-Z]/;


	     if(pass.length < 6) {
	       $(some).siblings('.msg').text("Password must contain at least six characters!").slideDown();
	       $(some).removeClass('correct').addClass("wrong");
	       return false;
	     }
	     else if(!re1.test(pass)) {
	       $(some).siblings('.msg').text("Password must contain at least one number (0-9)").slideDown();
	       $(some).removeClass('correct').addClass("wrong");
	       return false;
	     }
	     else if(!re2.test(pass)) {
	       $(some).siblings('.msg').text("Password must contain at least one lowercase letter (a-z)!").slideDown();
	       $(some).removeClass('correct').addClass("wrong");
	       return false;
	     }
	     else if(!re3.test(pass)) {
	        $(some).siblings('.msg').text("Password must contain at least one uppercase letter (A-Z)!").slideDown();
	        $(some).removeClass('correct').addClass("wrong");
	        return false;
	     }
	     else{
	     	$(some).siblings('.msg').slideUp('fast');
	     	$(some).removeClass('wrong').addClass("correct");
	     	return true;
	     }
	}
}

$("#password").blur(function(){
	checkSignInPassword(this);
});
$("#email").blur(function(){
	checkSignInEmail(this);
});

$("#name").blur(function(){
	checkName(this);
});
$("#signUpemail").blur(function(){
	checkSignUpEmail(this);
});
$("#signUppassword").blur(function(){
	checkSignUpPassword(this);
});

/// show pass for sign in 
function showSignInPassword(some){
	if($(some).hasClass('showing')){
		$("#password").prop("type","password");
		$(some).removeClass("showing").removeClass('fa-eye-slash').addClass('fa-eye');
	}
	else{
		$("#password").prop("type","text");
		$(some).addClass("showing").removeClass('fa-eye').addClass('fa-eye-slash');
	}
}
//sign up show pass 
function showSignUpPassword(some){
	if($(some).hasClass('showing')){
		$("#signUppassword").prop("type","password");
		$(some).removeClass("showing").removeClass('fa-eye-slash').addClass('fa-eye');
	}
	else{
		$("#signUppassword").prop("type","text");
		$(some).addClass("showing").removeClass('fa-eye').addClass('fa-eye-slash');
	}
}

function onSubmitForm(some){
	checkSignInPassword($("#password"));
	checkSignInEmail($("#email"));
	
	if(chkFormValid() == true){//// Form Validation Successful 
		$("#signInMsg").text("Successfully Submitted.").css("color","#090").slideDown();
		//$("#signInForm")[0].reset();
	}

	else{ ////Form validation Unsuccessful 
		$("#signInMsg").text("Remove form errors.").css("color","#900").slideDown();
	}
}

function onSignUpSubmitForm(some){
	checkName($("#name"));
	checkSignUpPassword($("#signUppassword"));
	checkSignUpEmail($("#signUpemail"));
	
	if(chkSignUpFormValid() == true){ //// Form Validation Successful 
		$("#signUpMsg").text("Successfully Registered.").css("color","#090").slideDown();
	}

	else{ ////Form validation Unsuccessful 
		$("#signUpMsg").text("Remove form errors.").css("color","#900").slideDown();
	}
}
function inSwitch(){
	$("#signUpModalCenter").modal("hide");
	$("#exampleModalCenter").modal("show");
}
function upSwitch(){
	$("#exampleModalCenter").modal("hide");
	$("#signUpModalCenter").modal("show");
}