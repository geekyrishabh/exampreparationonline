<?php
include 'DB.php';
$dao = new DB();
$table_name = "ask_your_seniors_answers";

$user_answer = array(
            'answer' => $_POST['answer_value'],
            'questionid' => $_POST['question_id'],
            'posted_by'=>""
        );

$insert = $dao->insert($table_name,$user_answer);
if($insert){
    $data['data'] = $insert;
    $data['status'] = 'OK';
    $data['msg'] = 'User answer has been added successfully.';
}else{
    $data['status'] = 'ERR';
    $data['msg'] = 'Some problem occurred, please try again.';
}
echo json_encode($data);


?>