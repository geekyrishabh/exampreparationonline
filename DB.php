<?php
class DB{
   
   private $dbHost='localhost';
   private $dbUsername='root';
   private $dbPassword='';
   private $dbName='questionanswers';
   public $db;
   
   
   public function __construct(){
           if(!isset($this->db)){
               // Connect to the database
               try{
                   $conn = new PDO("mysql:host=".$this->dbHost.";dbname=".$this->dbName, $this->dbUsername, $this->dbPassword);
                   $conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                   $this->db = $conn;
                   //echo "connection success";
               }catch(PDOException $e){
                   die("Failed to connect with MySQL: " . $e->getMessage());
               }
           }
       }


    public function insert($table,$data){
        if(!empty($data) && is_array($data)){
            $columns = '';
            $values  = '';

            $columnString = implode(',', array_keys($data));
            $valueString = ":".implode(',:', array_keys($data));
            $sql = "INSERT INTO ".$table." (".$columnString.") VALUES (".$valueString.")";
            $query = $this->db->prepare($sql);
            foreach($data as $key=>$val){
//         $val = htmlspecialcharsstrip_tags($val));
                $query->bindValue(':'.$key, $val);
            }
            //  try{
            //   $insert = $query->execute();
            // }
            // catch(PDOException $e){
            //   echo 'Exception -> ';
            //   var_dump($e->getMessage());
            // }
            $insert = $query->execute();
            if($insert){
                $data['id'] = $this->db->lastInsertId();
                return $data;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
	 
	 	public function getRows($table,$conditions = array()){
	 	  $sql = 'SELECT ';
	 	  $sql .= array_key_exists("select",$conditions)?$conditions['select']:'*';
	 	  $sql .= ' FROM '.$table;
	 	  if(array_key_exists("where",$conditions)){
	 	    $sql .= ' WHERE ';
	 	    $i = 0;
	 	    foreach($conditions['where'] as $key => $value){
	 	      $pre = ($i > 0)?' AND ':'';
	 	      $sql .= $pre.$key." = '".$value."'";
	 	      $i++;
	 	    }
	 	  }

	 	  if(array_key_exists("order_by",$conditions)){
	 	    $sql .= ' ORDER BY '.$conditions['order_by']; 
	 	  }

	 	  if(array_key_exists("start",$conditions) && array_key_exists("limit",$conditions)){
	 	    $sql .= ' LIMIT '.$conditions['start'].','.$conditions['limit']; 
	 	  }
	 	  elseif(!array_key_exists("start",$conditions) && array_key_exists("limit",$conditions)){
	 	    $sql .= ' LIMIT '.$conditions['limit']; 
	 	  }
//	 	  echo $sql;
	 	  $query = $this->db->prepare($sql);
	 	  $query->execute();

	 	  if(array_key_exists("return_type",$conditions) && $conditions['return_type'] != 'all'){
	 	    switch($conditions['return_type']){
	 	      case 'count':
	 	      $data = $query->rowCount();
	 	      break;
	 	      case 'single':
	 	      $data = $query->fetch(PDO::FETCH_ASSOC);
	 	      break;
	 	      default:
	 	      $data = '';
	 	    }
	 	  }else{
	 	    if($query->rowCount() > 0){
	 	      $data = $query->fetchAll(PDO::FETCH_ASSOC);
	 	    }
	 	  }
	 	  return !empty($data)?$data:false;
	 	}
		
		public function getdatabyid($table ,$id)
		{
			global $con;
			$con=mysqli_connect($this->dbHost,$this->dbUsername,$this->dbPassword);
			// echo $con;
			$tablenew= "`".$table."`";
			$rs=$con ->query( "select * from ".$tablenew."where `id`=".$id);
			$row= $rs->fetchAll();
			return $row;
			
		}
		public function update($table,$data,$id)
		{
			
			$tablenew= "`".$table."`";
		$data=array('flag'=>1); 
		
			$sql= "UPDATE".$tablenew." SET `flag`='".$data['flag']."' WHERE `answerid`='".$id."'";
			$query = $this->db->prepare($sql);
			$insert=$query->execute();
			return $insert;
			}
		     // public function tojson($table)
       //  {
       //   global $con;
       //    $tablenew= "`".$table."`";
       //   $json_data =array();
       //   $rs=$con ->query( "select * from ".$tablenew );
       //   $fp = fopen('data.json', 'w+');
       //   while($res =$rs->fetch())
       //   {
       //       $json_array['id']=$res['id'];
       //       $json_array['name']=$res['name'];
       //       $json_array['express']=$res['express'];
       //       $json_array['about']=$res['about'];
                
       //       array_push($json_data,$json_array); 
       //      }
       //        $fp = fopen('data.json', 'w+');
       //           fwrite($fp, json_encode($json_data));
       //                fclose($fp);

       //  }	
		public function deletedatabyid($table,$id)
		{
			global $con;
			$tablenew= "`".$table."`";
			$sql="DELETE FROM ".$tablenew." WHERE `id`=".$id;
			$delete=$con->exec($sql);
			return $delete;
			
		}
}
?>


