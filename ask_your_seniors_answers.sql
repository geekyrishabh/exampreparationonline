-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 23, 2018 at 11:06 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `questionanswers`
--

-- --------------------------------------------------------

--
-- Table structure for table `ask_your_seniors_answers`
--

DROP TABLE IF EXISTS `ask_your_seniors_answers`;
CREATE TABLE IF NOT EXISTS `ask_your_seniors_answers` (
  `answerid` int(255) NOT NULL AUTO_INCREMENT,
  `questionid` int(255) NOT NULL, 
  FOREIGN KEY (questionid) REFERENCES ask_your_seniors_questions(questionid),
  `answer` varchar(500) NOT NULL,
  `posted_by` varchar(100) NOT NULL,
  `flag` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answerid`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ask_your_seniors_answers`
--

INSERT INTO `ask_your_seniors_answers` (`answerid`, `questionid`, `answer`, `posted_by`, `flag`) VALUES
(1, 1, 'see google', 'rishabh', 1),
(2, 1, 'see interviewbit', 'rishabh', 1),
(4, 2, 'go to tutor and ask', 'rb', 1),
(7, 2, 'rkkkk', 'rishabh', 1),
(22, 1, 'see their site', '', 1),
(26, 1, 'testing', '', 0),
(25, 1, 'tt', '', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
