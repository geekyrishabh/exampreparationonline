-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 23, 2018 at 11:07 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `questionanswers`
--

-- --------------------------------------------------------

--
-- Table structure for table `ask_your_seniors_questions`
--

DROP TABLE IF EXISTS `ask_your_seniors_questions`;
CREATE TABLE IF NOT EXISTS `ask_your_seniors_questions` (
  `questionid` int(50) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `flag_has_answer` int(2) NOT NULL DEFAULT '0',
  `tag_hr_rounds` tinyint(1) DEFAULT '0',
  `tag_gd` int(2) NOT NULL DEFAULT '0',
  `tag_aptitude` int(2) NOT NULL DEFAULT '0',
  `tag_analytical_question` int(2) NOT NULL DEFAULT '0',
  `tag_core_topics` int(2) NOT NULL DEFAULT '0',
  `tag_on_campus` int(2) DEFAULT '0',
  `tag_off_campus` int(2) NOT NULL DEFAULT '0',
  `tag_product_based_company` int(2) NOT NULL DEFAULT '0',
  `tag_service_based_company` int(2) NOT NULL DEFAULT '0',
  `tag_software` int(2) NOT NULL DEFAULT '0',
  `tag_airline` int(2) NOT NULL DEFAULT '0',
  `tag_airhostess` int(2) NOT NULL DEFAULT '0',
  `tag_pilot` int(2) NOT NULL DEFAULT '0',
  `tag_healthcare` int(2) NOT NULL DEFAULT '0',
  `tag_automobile` int(2) NOT NULL DEFAULT '0',
  `tag_banking` int(2) NOT NULL DEFAULT '0',
  `tag_teaching` int(2) NOT NULL DEFAULT '0',
  `tag_media_and_entertainment` int(2) NOT NULL DEFAULT '0',
  `tag_airlines` int(2) NOT NULL DEFAULT '0',
  `organisation_name` text NOT NULL,
  `flag_validation` int(2) NOT NULL DEFAULT '0',
  `tag_work_from_home` int(2) NOT NULL DEFAULT '0',
  `tag_intern` int(2) NOT NULL DEFAULT '0',
  `tag_college_admission` int(2) NOT NULL DEFAULT '0',
  `tag_college_advice` int(2) NOT NULL,
  PRIMARY KEY (`questionid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ask_your_seniors_questions`
--

INSERT INTO `ask_your_seniors_questions` (`questionid`, `question`, `flag_has_answer`, `tag_hr_rounds`, `tag_gd`, `tag_aptitude`, `tag_analytical_question`, `tag_core_topics`, `tag_on_campus`, `tag_off_campus`, `tag_product_based_company`, `tag_service_based_company`, `tag_software`, `tag_airline`, `tag_airhostess`, `tag_pilot`, `tag_healthcare`, `tag_automobile`, `tag_banking`, `tag_teaching`, `tag_media_and_entertainment`, `tag_airlines`, `organisation_name`, `flag_validation`, `tag_work_from_home`, `tag_intern`, `tag_college_admission`, `tag_college_advice`) VALUES
(2, 'what is cutoff of dtu?', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'DTU', 1, 0, 0, 0, 0),
(1, 'job procedure for amazon', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'ymca', 1, 1, 1, 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
