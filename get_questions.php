<?php
include 'DB.php';
$dao = new DB();
$data = json_decode(file_get_contents("php://input"));
$table_name = "ask_your_seniors_questions";
$select_values = "question,questionid,flag_has_answer";

if(isset($data->d)) {
    $question_tags = array();
    $question_tags=$data->d;
 
  for ($i = 0; $i < sizeof($question_tags); $i++) {
        
        $retrieve_ques_condition[$question_tags[$i]] = 1;
   
    }

    $conditions = array('where' => $retrieve_ques_condition, 'select' => $select_values);

    $question_data['records'] = $dao->getRows($table_name, $conditions);
    echo json_encode($question_data);


}
?>