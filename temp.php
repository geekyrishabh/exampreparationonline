
<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
  <meta name="theme-color" content="#333333" />
  <meta name="viewport" content="width=device-width , initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge"> 
  <title>Template</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-beta1/katex.min.css" integrity="sha384-VEnyslhHLHiYPca9KFkBB3CMeslnM9CzwjxsEbZTeA21JBm7tdLwKoZmCt3cZTYD" crossorigin="anonymous">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-beta1/katex.min.js" integrity="sha384-O4hpKqcplNCe+jLuBVEXC10Rn1QEqAmX98lKAIFBEDxZI0a+6Z2w2n8AEtQbR4CD" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-beta1/contrib/auto-render.min.js" integrity="sha384-IiI65aU9ZYub2MY9zhtKd1H2ps7xxf+eb2YFG9lX6uRqpXCvBTOidPRCXCrQ++Uc" crossorigin="anonymous"></script>
  <link href="https://fonts.googleapis.com/css?family=Jura:500,700|Montserrat:400,500" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

  <link href="./css/styles.css" rel="stylesheet">
  <style>
  </style>
</head>
<body id="myBody" ng-controller="myCtrl">


<nav class="navbar navbar-expand-sm bg-dark navbar-dark font">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="#">
    ExamOnline
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link"  data-toggle="modal" data-target="#exampleModalCenter">Sign In</a>
      </li>
      <li class="nav-item">
        <a class="nav-link"   data-toggle="modal" data-target="#signUpModalCenter">Sign Up</a>
      </li> 
    </ul>
  </div> 
</nav>

<!--Sign In  Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content font">
      <div class="modal-header font">
        <h5 class="modal-title font" id="exampleModalLongTitle">Sign In</h5>
        <div style="position: absolute; display: flex; top: 73px; left: 5%; text-align:  center; width: 90%; padding: 0; ">
          <small id="signInMsg" class="formMsg">
            Form message.
          </small>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="fas fa-times" aria-hidden="true">
            </span>
        </button>
      </div>
      <div class="modal-body">
        <div class="myFormBox font">
          <form id="signInForm" autocomplete="off" method="POST" onsubmit="onSubmitForm(this);">
            <div>
              <input id="email" type="email" ng-model="login_email" name="email" required="" onkeyup="checkSignInEmail(this);" class="">
              <label>Email</label>
              <p id="emailMsg" class="msg"></p>
              <span id="emailMsgIco" class="msgIcon fas "></span>
            </div>
            <div>
              <input id="password" type="password" ng-model="login_password" name="password" required="" onkeyup="checkSignInPassword(this);">
              <label>Password</label>
              <p id="passMsg" class="msg"></p>
              <span id="passwordIco" class="msgIcon fas fa-eye" onclick="showSignInPassword(this)"></span>
            </div>
            <div class="chkDiv">
              <label class="container">Remember me
                <input type="checkbox" name="remember">
                <span class="checkmark"></span>
              </label>
            </div>
            <div>
              <input class="btn btn-primary  btn-default btn-block btn-round"  type="submit" name="submit" value="Sign In" ng-click="login1(login_email,login_password)" /> 
              <span class="signInIco fas fa-arrow-right">
              </span>
            </div>
          </form>
          <div class="text-center">
            <small style="margin: 0; ">
              <a class="forgetLink faded" href="#">
                Forget Password ?
              </a>
            </small>
          </div>
          <div style="display: flex;position: relative;">
              <hr width="45%" style="margin-left: 0;border-color: #343a40;"/>
              <span style="    position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);">OR</span>
              <hr width="45%" style="margin-right: 0;border-color: #343a40;"/>
          </div>
          <div class="socialLinks" style="margin: 10px auto; ">
            <div style="position:  relative; margin: 0 auto 20px auto; ">
              <button class="btn btn-social btn-fb btn-round" style="position: inherit; top: 0%; left: 0%; ">
                <span>
                </span>
              </button>
              <button class="btn btn-social btn-g btn-round" style="position: absolute; top: 0%; right: 0%; ">
                <span>         
                </span>Google
              </button>                       
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div style="margin: auto; font-size: 0.75em; text-align: center; color: white; ">
          Don't have an account ?
          <a class="createLink" href="#" onclick="upSwitch();">
            Create an account
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Sign Up  Modal -->
<div class="modal fade" id="signUpModalCenter" tabindex="-1" role="dialog" aria-labelledby="signUpModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content font">
      <div class="modal-header font">
        <h5 class="modal-title font" id="signUpModalLongTitle">Sign Up</h5>
        <div style="position: absolute; display: flex; top: 73px; left: 5%; text-align:  center; width: 90%; padding: 0; ">
          <small id="signUpMsg" class="formMsg">
            Form message.
          </small>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span class="fas fa-times" aria-hidden="true">
          </span>
        </button>
      </div>
      <div class="modal-body">
        <div class="myFormBox font">
          <form id="signUpForm" autocomplete="off" method="POST" onsubmit="onSignUpSubmitForm(this);">
            <div>
              <input id="name" type="name" name="signUpName" required="" onkeyup="checkName(this);" class="" autocomplete="off" ng-model="name">
              <label>Name</label>
              <p id="nameMsg" class="msg"></p>
              <span id="nameMsgIco" class="msgIcon fas "></span>
            </div>
            <div>
              <input id="signUpemail" type="email" name="registerEmail" required="" onkeyup="checkSignUpEmail(this);" class="" autocomplete="off" ng-model="email">
              <label>Email</label>
              <p id="signUpemailMsg" class="msg"></p>
              <span id="signUpemailMsgIco" class="msgIcon fas "></span>
            </div>
            <div>
              <input id="signUppassword" type="password" name="registerPassword" required="" onkeyup="checkSignUpPassword(this);" ng-model="password">
              <label>Password</label>
              <p id="passMsg" class="msg"></p>
              <span id="signUpPasswordIco" class="msgIcon fas fa-eye" onclick="showSignUpPassword(this)"></span>
            </div>
            <div>
              <input class="btn btn-primary  btn-default btn-block btn-round" type="submit"  ng-click="register1(name,email,password)" name="submit" value="REGISTRATION" >
              <span class="signInIco fas fa-arrow-right"></span>
            </div>
          </form>
          <div style="display: flex;position: relative;">
            <hr width="45%" style="margin-left: 0;border-color: #343a40;"/>
            <span style="    position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);">OR</span>
            <hr width="45%" style="margin-right: 0;border-color: #343a40;"/>
          </div>
          <div class="socialLinks" style="margin: 10px auto; ">
            <div style="position:  relative; margin: 0 auto 20px auto; ">
              <button class="btn btn-social btn-fb btn-round" style="position: inherit; top: 0%; left: 0%; ">
                <span>
                </span>
              </button>
              <button class="btn btn-social btn-g btn-round" style="position: absolute; top: 0%; right: 0%; ">
                <span></span>
                Google
              </button>      
            </div>      
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div style="margin: auto; font-size: 0.75em; text-align: center; color: white; ">
          Already registered ?
          <a class="createLink" href="#" onclick="inSwitch();">
            Sign in here
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="nameBar container-fluid">
  
  <!--%%%%%%%%%%%%%  What I call it? a Name BAr ? %%%%%%%%-->

  <div class="row header font" style="position:  relative;/* display: -webkit-box; */">
    <div class="col-xs-3 col-sm-1" style="padding: 5px;">
      <div class="image " style="margin-right: 0;">
        <img src="./img/avatar.png">
      </div>
    </div>
    <div class="col-xs-12 col-sm-5  name" style="padding-left:10px;">
      <div> 
        <p class="" style="    margin:0 auto;">HELLO</p> 
        <p style="text-transform:uppercase;font-weight:bold;margin: 0; ">Joe Smith</p>
      </div>
    </div>
    <div style="/* padding: 0; */" class="col-xs-9 col-sm-6">
      <div class="headerRight">
        <div class="circle red" style="/* margin-left: 20px; *//* margin-right: 20px; */">
          <span class="inside bold">
            10
          </span>
        </div>
        <div style="display: inherit;margin-left: 10px;">
          <div class="verticalCenter" style="display:flex;">
            <div>SORT BY : </div>
            <div class="bold"> LATEST</div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="container-fluid"">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1" >
      Content goes here 
      <div>
        <ul>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </li>
        </ul>
    </div>
    </div>
  </div>
</div>


<script> ////// ANGULAR
  var app = angular.module('myApp', []);
  app.controller('myCtrl',function($scope,$http)
  {

  });
$(function(){
  $(window).on('scroll', function(event) {
    var scrollValue = $(window).scrollTop();
    $("#sc").text(scrollValue+"px");
    if (scrollValue > 55) {
      $('.nameBar').addClass('affix');
    } 
    else{
      $('.nameBar').removeClass('affix');
    }
  });
});
  
</script>
<script type="text/javascript" src="validater.js"></script>
</body>
</html>